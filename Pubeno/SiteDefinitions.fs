// Copyright (c) 2018 Theodore Tsirpanis
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

namespace Pubeno

open WebSharper
open WebSharper.UI.Client
open WebSharper.UI.Templating
open Pubeno.Core

[<JavaScript>]
type PubenoTemplate = Template<"wwwroot/index.html", ClientLoad.FromDocument>

[<JavaScript>]
module SiteDefinitions =
    let empty = Blank ""

    type SubjectMap = Map<string, CheckedInput<float>>

    type SSGC =
        | NotTaken
        | GC1
        | GC2
        with
            static member All = [NotTaken; GC1; GC2]
            member private x.Subject =
                match x with
                | NotTaken -> None
                | GC1 -> Some -1.0
                | GC2 -> Some 0.0
                |> Option.map (fun gc -> {Name = "Ειδικό μάθημα"; GravityCoefficient = Some gc})
            member x.FuseGrade g =
                match x.Subject, g with
                // The user does not take an exam at a special subject.
                // In this case, the operation succeeds, but there is no additional subject.
                | None, _ -> Some None
                // The user does take an exam at a special subject, but has not provided a grade.
                // The operation fails.
                | Some _, None -> None
                // The user has provided both a special subject and a grade.
                // The operation succeeds, and returns a pair of these two.
                | Some ss, Some g -> (ss, g) |> Some |> Some
            override x.ToString() =
                match x with
                | NotTaken -> "Δεν το δίνω"
                | GC1 -> "Συντελεστής Βαρύτητας: 1.0"
                | GC2 -> "Συντελεστής Βαρύτητας: 2.0"

    type PubenoModel = {
        Field: AvailableFields
        Grades: SubjectMap
        SpecialGC: SSGC
        SpecialGrade: CheckedInput<float>
        Target: CheckedInput<int>
    }
    with
        static member Default =
            {
                Field = Humanities
                Grades = Map.empty
                SpecialGC = NotTaken
                SpecialGrade = empty
                Target = empty
            }