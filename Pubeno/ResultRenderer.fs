// Copyright (c) 2018 Theodore Tsirpanis
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

namespace Pubeno

open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open Pubeno.Core
open Pubeno.Core.FuzzyEstimator

[<JavaScript>]
module ResultRenderer =

    open SiteDefinitions

    let private resultClasses isPositive =
        [
            "is-size-1-desktop"
            "is-size-2-tablet"
            "is-size-4-mobile"
            (if isPositive then
                "has-text-success"
            else
                "has-text-danger")
        ]
        |> String.concat " "

    type private PubenoResult =
        | SimpleResult of int
        | OneUnknownResult of Estimation1Result * unknownSubject:Subject * target:int

    let private calculateResult (m: PubenoModel) = maybe {
        let getGrade =
            function
            // The subject exists in the model and is a valid number.
            // It is considered as known
            | Some (Valid (x, _)) -> x |> Grade.Create |> Option.map Some
            // The subject either exists in the model and has an empty grade, or does not exist at all.
            // It is considered as unknown
            | Some (Blank _) | None -> Some None
            // The subject exists in the model, but has an invalid grade.
            // The operation fails.
            | Some (Invalid _) -> None
        let! specialSubjectGrade = m.SpecialGrade |> Some |> getGrade |> m.SpecialGC.FuseGrade
        let! subjects =
            m.Field.Field.Subjects
            |> List.map (fun s -> m.Grades.TryFind s.Name |> getGrade |> Option.map (fun g -> s, g))
            |> List.collectAll
            |> Option.map
                (
                    List.consMaybe specialSubjectGrade
                    >> List.partitionMap (snd >> Option.isSome) (fun (x1, x2) -> x1, Option.defaultValue Grade.Min x2)
                    >> (fun (a, b) -> a, List.map fst b)
                )
        Logging.log <| sprintf "Subjects: %A" subjects
        match subjects with
        | allKnown, [] -> return Calculator.calculateScore allKnown |> SimpleResult
        | someKnown, [oneUnknown] ->
            let! target =
                match m.Target with
                | Valid (x, _) when x <= 21000 && x >= 0 -> Some x
                | _ -> None
            return (estimate1 oneUnknown someKnown target, oneUnknown, target) |> OneUnknownResult
        | _ -> return! None
    }

    let ResultDoc m =
        let result = calculateResult m
        Logging.log <| sprintf "Calculation result: %A" result
        match result with
        | Some x ->
            match x with
            | SimpleResult x -> PubenoTemplate.ResultSimple().Score(string x).Elt(), true
            | OneUnknownResult (x, {Name = sub}, target) ->
                match x with
                | AlreadyAchieved -> PubenoTemplate.ResultFuzzy1Already().Score(string target).SubjectName(sub).Elt(), true
                | Feasible g -> PubenoTemplate.ResultFuzzy1Feasible().Grade(string g.Value).SubjectName(sub).Elt(), true
                | Infeasible _ -> PubenoTemplate.ResultFuzzy1Infeasible().Score(string target).SubjectName(sub).Elt(), false
        | None -> Html.div [] [], false
        |> (fun (doc, pos) -> doc.SetAttribute("class", resultClasses pos); doc :> Doc)
