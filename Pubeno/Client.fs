// Copyright (c) 2018 Theodore Tsirpanis
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

namespace Pubeno

open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open WebSharper.UI.Html
open WebSharper.Mvu
open Pubeno.Core
open WebSharper.JavaScript

[<JavaScript>]
module Client =

    open SiteDefinitions

    [<NamedUnionCases("type")>]
    type PubenoMessage =
        | FieldChange of newField:AvailableFields
        | GradeChange of subject:string * newGrade:CheckedInput<float>
        | SSGCChange of newSSGC:SSGC
        | SSGradeChange of newSSGrade:CheckedInput<float>
        | TargetChange of newTarget:CheckedInput<int>

    let Update msg model =
        match msg with
        | FieldChange f -> {model with Field = f}
        | GradeChange (name, newGrade) -> {model with Grades = Map.add name newGrade model.Grades}
        | SSGCChange NotTaken -> {model with SpecialGC = NotTaken; SpecialGrade = empty}
        | SSGCChange x -> {model with SpecialGC = x}
        | SSGradeChange x when model.SpecialGC = NotTaken -> {model with SpecialGC = GC1; SpecialGrade = x}
        | SSGradeChange x -> {model with SpecialGrade = x}
        | TargetChange x -> {model with Target = x}

    let GradeInputs (m: View<PubenoModel>) (fGetSubjectVar: string -> Var<CheckedInput<float>>) =
        V(
            m.V.Field.Field.Subjects
            |> Seq.map (fun s -> s, fGetSubjectVar s.Name))
        |> Doc.BindSeqCachedBy (fun (s, _) -> s.Name) (fun (s, v) -> PubenoTemplate.GradeInput().SubjectName(s.Name).GradeVar(v).Doc())

    let Render dispatch (model: View<PubenoModel>) =
        let fGetSubjectVar s =
            Var.Make (V (model.V.Grades.TryFind(s) |> Option.defaultValue empty)) (fun g -> GradeChange (s, g) |> dispatch)
        let ssgcVar = Var.Make (V model.V.SpecialGC) (SSGCChange >> dispatch)
        let ssGradeVar = Var.Make (V model.V.SpecialGrade) (SSGradeChange >> dispatch)
        let targetVar = Var.Make (V model.V.Target) (TargetChange >> dispatch)
        PubenoTemplate()
            .Result(Doc.BindView ResultRenderer.ResultDoc model)
            .FieldSelect(Doc.Select [attr.id "fieldselect"] (fun (x: AvailableFields) -> x.Name) AvailableFields.All (Var.Make (V model.V.Field) (FieldChange >> dispatch)))
            .Grades(GradeInputs model fGetSubjectVar)
            .SpecialSubjcectGC(Doc.Select [] string SSGC.All ssgcVar)
            .SpecialSubjectVar(ssGradeVar)
            .TargetVar(targetVar)
            .Bind()

    /// When it is changed, the local storage resets.
    /// It is changed whenever there are breaking changes to the model.
    let [<Literal>] VersionTag = "A6406428-370E-4FB8-BA8F-02300CB338A0"

    [<SPAEntryPoint>]
    let Main () =
        if JS.Window.LocalStorage.GetItem "VersionTag" <> VersionTag then
            JS.Window.LocalStorage.RemoveItem "pubeno"
        JS.Window.LocalStorage.SetItem("VersionTag", VersionTag)
        App.CreateSimple PubenoModel.Default Update Render
        |> App.WithLocalStorage "pubeno"
        #if DEBUG
        |> App.WithRemoteDev (RemoteDev.Options(hostname = "localhost", port = 8000))
        #endif
        |> App.Run
