// Copyright (c) 2018 Theodore Tsirpanis
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

namespace Pubeno.Core

open WebSharper.JavaScript

/// Some monoids in the category of endofunctors.
[<AutoOpen>]
module Monads =

    /// Some boilerplate that would be avoided had F# been more like Haskell.
    type MaybeBuilder() =
        member __.Zero() = Some ()
        /// The return you know.
        member __.Return m = Some m
        /// 😵
        member __.Delay f = f()
        /// >>= in disguise.
        member __.Bind (m, f) = Option.bind f m
        member x.Combine m = x.Bind m
        /// (a + b)⁵ = a⁵ + 5a⁴b + 10a³b² + 10a²b³ + 5ab⁴ + b⁵
        member __.Run m = m
        /// (a + b)⁴ = a⁴ + 4a³b + 6a²b² + 4ab³ + b⁴
        member __.ReturnFrom m = m

    /// Your favorite maybe monad.
    /// Call it.
    let maybe = MaybeBuilder()

/// Logging utilities
module Logging =

    let log =
        #if DEBUG
        Console.Log
        #else
        ignore
        #endif
        
    let logf x = sprintf x |> log

/// List utilities
module List =
    /// Returns a list if all its elements are `Some` and `None` otherwise.
    let rec collectAll =
        function
        | (Some x) :: xs ->
            xs |> collectAll |> Option.map (fun xs -> x :: xs)
        | None :: _ -> None
        | [] -> Some []

    /// Prepends an element to a list. Maybe.
    let consMaybe x xs =
        match x with
        | Some x -> x :: xs
        | None -> xs

    /// Partitions a list and transforms its elements before returning them.
    let partitionMap pred f x =
        let x1, x2 = List.partition pred x
        let f = List.map f
        f x1, f x2
