// Copyright (c) 2018 Theodore Tsirpanis
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

namespace Pubeno.Core

open WebSharper

/// The greek names of each subject.
module SubjectNames =

    /// New Greek: An all-arounder; you can't escape from her... 😈
    let newGreek = "Έκθεση"
    /// Physics: Rigid body mechanics and so on... I can no longer have an opinion on this subject. Ditto with Chemistry.
    let physics = "Φυσική"
    /// Chemistry: More theory than any maths. Le Chatelier and his friends came here. I can no longer have an opinion on this subject. Ditto with Physics.
    let chemistry = "Χημεία"
    /// Maths: Why do they teach us the antiderivative, and NOT the indefinite integral? Still, IMHO the best of the subjects listed here.
    let maths = "Μαθηματικά"
    /// Biology: DNA, mutations, genetic diseases and so on...
    let ``biology++`` = "Βιολογία Προσανατολισμού"
    /// Biology again: Like the above, but looks easier. All students are taught it, despite not many of them being examined.
    let biology = "Βιολογία Γενικής"
    /// ADPE (Application Development on Programming Environment): An insult to all programmers out there. Imagine Pascal with Greek keywords, only worse. 🤮
    let adpe = "Α.Ε.Π.Π."
    /// ETF (Economic Theory Fundamentals): The problem is the theory. The rest is very easy.
    let etf = "Α.Ο.Θ."
    /// Ancient Greek: Τω μεθύειν το εράν όμοιόν εστι. Ποιεί γαρ θερμούς και ιλαρούς και διακεχυμένους. Whatever that means.
    let ancientGreek = "Αρχαία"
    /// Latin: I know Latin. Just enough Latin to communicate with a person who doesn't know Latin.
    let latin = "Λατινικά"
    /// History: Refugees, Eleftherios Venizelos, and historical sources. Wasn't paying any actual attention at all. 😢
    let history = "Ιστορία"
    /// Special lessons. Used in vocational schools.
    let spec1 = "Μάθημα Ειδικότητας 1"
    /// Special lessons. Used in vocational schools.
    let spec2 = "Μάθημα Ειδικότητας 2"

open SubjectNames

/// The available scientific fields.
/// This type is mostly introduced for type safety with the app local storage
type AvailableFields =
    /// Humanities, Legal, and Social Sciences.
    /// I don't understand them. How they study or how they acquire this type knowledge in general.
    /// And I bet they feel the same for people like me.
    | [<Constant("humanities")>] Humanities
    /// Engineers, mathematicians, physicists... go here.
    /// I used to be there; I could go to Computer Science School from there as well.
    | [<Constant("science")>] Science
    | [<Constant("health-humanities")>] HealthHumanities
    /// 👨‍⚕️, vets, dentists, biologists... go here. Chemists can go here if they want as well.
    /// Why are they not examined at Maths? OK, "they" don't need them, but I still don't get it.
    | [<Constant("health-science")>] HealthScience
    | [<Constant("health-economics")>] HealthEconomics
    /// Economists, business administrators, computer scientists (🤩)... go here.
    /// Here I am now. Much easier than the 2nd field; I should have gone there from the beginning.
    | [<Constant("economics")>] Economics
    /// Vocational schools. The calculation algorithm differs a bit here...
    | [<Constant("vocational")>] Vocational
    with
        /// Gets the `Field` associated with each option.
        member x.Field =
            let mkField = Field.CreateSimple
            match x with
            | Humanities -> mkField "Ανθρωπιστικές, Νομικές και Κοινωνικές Επιστήμες" (ancientGreek, 1.3) (history, 0.7) latin newGreek
            | Science -> mkField "Θετικές και Τεχνολογικές Επιστήμες" (maths, 1.3) (physics, 0.7) chemistry newGreek
            | HealthHumanities -> mkField "Επιστήμες Υγείας και Ζωής - Θεωρητική" (biology, 0.9) (newGreek, 0.4) ancientGreek history
            | HealthScience -> mkField "Επιστήμες Υγείας και Ζωής - Θετική" (``biology++``, 1.3) (chemistry, 0.7) physics newGreek
            | HealthEconomics -> mkField "Επιστήμες Υγείας και Ζωής - Οικονομικά" (biology, 0.9) (newGreek, 0.4) adpe maths
            | Economics -> mkField "Επιστήμες Οικονομίας και Πληροφορικής" (maths, 1.3) (etf, 0.7) adpe newGreek
            | Vocational -> 
                let mkSubject name gc = {Subject.Name = name; GravityCoefficient = Some gc}
                {
                    Name = "ΕΠΑΛ"
                    Subjects =
                        [
                            newGreek, -0.5
                            maths, -0.5
                            spec1, 1.5
                            spec2, 1.5
                        ] |> List.map ((<||) mkSubject)
                }

        /// Gets the name of the underlying field
        member x.Name = x.Field.Name

        /// A list of all scientific fields.
        /// Some schools (like 👮, 👨‍🚒, 👨‍🏫, and the emojiless military ones) can be accessed by either of them.
        static member All = [Humanities; Science; HealthHumanities; HealthScience; HealthEconomics; Economics; Vocational]
