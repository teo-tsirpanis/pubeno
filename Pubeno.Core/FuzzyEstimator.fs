// Copyright (c) 2018 Theodore Tsirpanis
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

/// This module is about the candidates that do not know how much they will write at some subjects.
module Pubeno.Core.FuzzyEstimator

/// The result of an estimation with one unknown subject.
type Estimation1Result =
    /// The candidate has already achieved his gole no matter what he writes.
    | AlreadyAchieved
    /// The candidate can achieve his goal if he writes at least the given grade at this subject.
    | Feasible of Grade
    /// The candidate cannot achieve his goal, unless he writes more than 20... 😛
    | Infeasible of float

/// Makes an estimation about a candidate who does not know his performance on `s`,
/// has written `subs` at the other subjects, and wants to score at least `target`.
let estimate1 (s: Subject) subs target =
    let totalFromTheRest = subs |> Calculator.calculateScore
    Logging.log <| sprintf "Total from the rest: %d" totalFromTheRest
    match target - totalFromTheRest with
    | x when x <= 0 -> AlreadyAchieved
    | x ->
        let mult = Calculator.makeRealGravityCoefficient s.GravityCoefficient
        let result = (float x) / mult
        Logging.log <| sprintf "Multiplier: %f Result: %f" mult result
        match Grade.Create result with
        | Some x -> Feasible x
        | None -> Infeasible result
