﻿// Copyright (c) 2018 Theodore Tsirpanis
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

namespace Pubeno.Core

open WebSharper

type Subject = {
    Name: string
    GravityCoefficient: float option
}
with
    static member Create name gc = {Name = name; GravityCoefficient = gc}

/// Each scientific field gives access to some universities.
/// Candidates must be examined at four lessons, the two of which matter more at the final calculation as determined in the double values.
/// This data structure contains the name of the field in Greek, as well as the Greek name of each lesson.
type Field = {
    Name: string
    Subjects: Subject list
}
with
    static member CreateSimple name (s1, gc1) (s2, gc2) s3 s4 =
            {
                Name = name
                Subjects =
                [
                    s1, Some gc1
                    s2, Some gc2
                    s3, None
                    s4, None
                ] |> List.map ((<||) Subject.Create)
            }

type Grade = private Grade of float
    with
        static member Min = Grade 0.0

        static member Max = Grade 20.0
        static member Create x =
            if x >= 0.0 && x <= 20.0 then
                (x |> ((*) 10.0) |> ceil) / 10.0 |> Grade |> Some
            else
                None

        member x.Value = match x with | Grade x -> x

        override x.ToString() = sprintf "%f" x.Value

/// Calculating the final score
module Calculator =

    /// Converts a gravity coefficient to the number that the grade will be multiplied with.
    let internal makeRealGravityCoefficient =
        Option.defaultValue 0.0
        >> (+) 2.0
        >> (function | 0.0 -> 2.0 | x -> x) // We don't want it to become zero by accident
        >> (*) 100.0

    /// Calculates the final score.
    /// The order of the grades must match the order, as they are declared at the field.
    let calculateScore grades =
        grades |> Seq.iter (fun (s, Grade g) -> Logging.log s; Logging.log g)
        grades
        |> Seq.sumBy (fun ({GravityCoefficient = gc}, Grade g) -> gc |> makeRealGravityCoefficient |> (*) g)
        |> round
        |> int

[<assembly: JavaScript>]
do()