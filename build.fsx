// Copyright (c) 2018 Theodore Tsirpanis
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#r "paket:
nuget Fake.Core.Target
nuget Fake.IO.FileSystem
nuget Fake.Dotnet.Cli //"

#load "./.fake/build.fsx/intellisense.fsx"

open Fake.Core
open Fake.DotNet
open Fake.IO

let siteOutput = "./Pubeno/wwwroot/"
let output = "./public/"

Target.description "Cleans the generated content"

Target.create "Clean" (fun _ ->
    DotNet.exec id "clean" "" |> ignore
    Shell.cleanDirs ["./public/"; "./Pubeno/wwwroot/Content"])

Target.description "Builds the site"

Target.create "Build" (fun _ ->
    DotNet.build id "Pubeno/Pubeno.fsproj")

Target.description "Copies the site to the \"public/\" folder, as required by GitLab Pages."

Target.create "CopyToOutput" (fun _ ->
    Shell.copyRecursive siteOutput output true |> Trace.logItems "Output file: ")

Target.description "The default build target; currently runs everything"

Target.createFinal "Default" ignore

open Fake.Core.TargetOperators

"Clean"
    ==> "Build"
    ==> "CopyToOutput"
    ==> "Default"

Target.runOrDefault "Default"